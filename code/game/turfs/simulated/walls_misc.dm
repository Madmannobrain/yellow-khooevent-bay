/turf/simulated/wall/cult
	name = "wall"
	desc = "The patterns engraved on the wall seem to shift as you try to focus on them. You feel sick"
	icon_state = "cult"
	walltype = "cult"

/turf/simulated/wall/wooden
	name = "wooden wall"
	desc = "A wooden wall."
	icon_state = "wood"
	walltype = "wooden"